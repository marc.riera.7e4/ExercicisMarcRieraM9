package Ex2;

public class Numero extends Thread {
    long valor;

    public Numero(long valor) {
        this.valor = valor;
    }

    @Override
    public void run() {
        boolean primer = true;
        for (long x = (valor - 1); x > 1; x--) {
            if (valor % x == 0) {
                primer = false;
                System.out.println("El numero " + valor + " no es primer");
                boolean sortir = false;
                long y = valor + 1;
                while (!sortir) {
                    for (long z = y - 1; z > 2; z--) {
                        primer=true;
                        if (y % z == 0) {
                            primer = false;
                            break;
                        }
                    }
                    if (primer == true){
                        System.out.println("El següent numero primer despres de "+valor+" es "+ y);
                        sortir=true;
                        primer=false;
                    }

                    y++;
                }
                break;
            }
        }
        if (primer == true) {
            System.out.println("El numero "+valor+" es primer");
        }
    }
}