package Camisas;

public class Camisa {
    private String id;
    private String model;
    private String talla;
    private String color;

    public Camisa(String id, String model, String talla, String color) {
        this.id = id;
        this.model = model;
        this.talla = talla;
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Camisa{" +
                "id='" + id + '\'' +
                ", model='" + model + '\'' +
                ", talla='" + talla + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
