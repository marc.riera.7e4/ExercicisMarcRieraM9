package Camisas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class StrategyPattern {
    public static void main(String[] args) {
        ArrayList<Camisa> filtrarCamises = new ArrayList<Camisa>();
        filtrarCamises.add(new Camisa("1","Camiseta", "L", "Vermell"));
        filtrarCamises.add(new Camisa("2","Blusa", "XL", "Verd"));
        filtrarCamises.add(new Camisa("3","Camiseta", "L", "Blau"));
        filtrarCamises.add(new Camisa("4","Esportiva", "M", "Vermell"));
        filtrarCamises.add(new Camisa("5","Blusa", "M", "Verd"));

        processElements(filtrarCamises, p->p.getModel().equals("Camiseta") && p.getTalla().equals("L"),p->p.getId(), nom-> System.out.println("La camisa "+nom));

        printCaracteristica(filtrarCamises, (Camisa p)->p.getColor().equals("Verd"));
    }

    private static void processElements(List<Camisa> filtrarCamises, Predicate<Camisa> tester, Function<Camisa, String> mapper, Consumer<String> block){
        for(Camisa p: filtrarCamises){
            if(tester.test(p)){
                String data=mapper.apply(p);
                block.accept(data);
            }
        }

    }
    private static void printCaracteristica(List<Camisa> llista, CaracteristicaPersona c){
        for(Camisa p: llista){
            if(c.test(p)) System.out.println(p);
        }
    }
    @FunctionalInterface
    interface CaracteristicaPersona{
        boolean test(Camisa p);
    }
}


