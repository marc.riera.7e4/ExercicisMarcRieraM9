package Ex1;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


class exRunnable {
    public static void main(String[] args) {
        Thread a1 = new Thread(new ArxiuThread("prova1.txt"));
        Thread a2 = new Thread(new ArxiuThread("prova2.txt"));
        Thread a3 = new Thread(new ArxiuThread("prova3.txt"));
        a1.start();
        a2.start();
        a3.start();
    }
}



class ArxiuRunnable implements Runnable {
    private String nom;

    public ArxiuRunnable(String nom) {
        this.nom = nom;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(nom));
            int lines = 0;
            while (true) {
                if (!(reader.readLine() != null)) break;
                lines++;
            }
            System.out.println("Numero de linies del arxiu "+ nom + ": "+ lines);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
