package Ex1;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class ArxiuThread extends Thread {
    private String nom;

    public ArxiuThread(String nom) {
        this.nom = nom;
    }

    @Override
    public void run() {
        try {
        BufferedReader reader = new BufferedReader(new FileReader(nom));
        int lines = 0;
        while (true) {
            if (!(reader.readLine() != null)) break;
            lines++;
        }
        System.out.println("Numero de linies del arxiu "+ nom + ": "+ lines);
        reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
